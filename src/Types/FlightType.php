<?php

declare(strict_types=1);

namespace Bartek\TripSorter\Types;

use Bartek\TripSorter\Coordinates\CoordinatesInterface;

class FlightType extends AbstractRegexType
{
    protected function getRegex(): string
    {
        return '#^'
            . 'flight_(?<number>[0-9]{3})_'
            . 'gate_(?<gate>[0-9]{2})_'
            . 'seat_(?<seat>[0-9]{3}(a|b|c))'
            . '(_bag_drop_(?<bagDrop>([0-9]{3})))?'
            . '$#';
    }

    public function describe(CoordinatesInterface $from, CoordinatesInterface $to, string $boardingCard): string
    {
        $matches = $this->getMatches($boardingCard);
        
        $result =  sprintf(
            'Take flight [%s] from %s to %s. Gate %s, seat %s.',
            $matches['number'],
            $from->getLabel(),
            $to->getLabel(),
            $matches['gate'],
            $matches['seat']
        );
        
        if (array_key_exists('bagDrop', $matches)) {
            $result .= "\nBaggage drop at ticket counter {$matches['bagDrop']}.";
        } else {
            $result .= "\nBaggage will we automatically transferred from your last leg.";
        }
        
        return $result;
    }
}
