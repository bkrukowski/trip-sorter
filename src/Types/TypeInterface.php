<?php

declare(strict_types=1);

namespace Bartek\TripSorter\Types;

use Bartek\TripSorter\Coordinates\CoordinatesInterface;

interface TypeInterface
{
    public function supports(string $boardingCardData): bool;
    
    public function describe(CoordinatesInterface $from, CoordinatesInterface $to, string $boardingCard): string;
}
