<?php

declare(strict_types=1);

namespace Bartek\TripSorter\Types;

abstract class AbstractRegexType implements TypeInterface
{
    public function supports(string $boardingCardData): bool
    {
        return !empty($this->getMatches($boardingCardData));
    }
    
    protected function getMatches(string $boardingCard): ?array
    {
        $matches = null;
        preg_match($this->getRegex(), mb_strtolower($boardingCard), $matches);
        
        return $matches;
    }

    abstract protected function getRegex(): string;
}
