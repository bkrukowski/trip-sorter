<?php

declare(strict_types=1);

namespace Bartek\TripSorter\Types;

use Bartek\TripSorter\Coordinates\CoordinatesInterface;

class TrainType extends AbstractRegexType
{
    protected function getRegex(): string
    {
        return '#^'
            . 'train_(?<number>[0-9]{3})_'
            . 'seat_(?<seat>([0-9]{3}(a|b|c))|(no))'
            . '$#';
    }

    public function describe(CoordinatesInterface $from, CoordinatesInterface $to, string $boardingCard): string
    {
        $matches = $this->getMatches($boardingCard);

        return sprintf(
            'Take train [%s] from %s to %s. %s',
            $matches['number'],
            $from->getLabel(),
            $to->getLabel(),
            'no' === $matches['seat'] ? 'No seat assignment.' : sprintf('Seat %s.', $matches['seat'])
        );
    }
}
