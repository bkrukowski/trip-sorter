<?php

declare(strict_types=1);

namespace Bartek\TripSorter\Connections;

use Bartek\TripSorter\Coordinates\SimpleCoordinates;
use Bartek\TripSorter\Types\FlightType;
use Bartek\TripSorter\Types\TrainType;
use Bartek\TripSorter\Types\TypeInterface;

class SimpleConnectionFactory
{
    /**
     * @var TypeInterface[]
     */
    private $types = [];
    
    public function __construct(TypeInterface... $types)
    {
        if (0 === count($types)) {
            $types = [
                new FlightType(),
                new TrainType()
            ];
        }
        
        $this->types = $types;
    }
    
    public function createConnection(string $from, string $to, string $boardingCard): ConnectionInterface
    {
        foreach ($this->types as $type) {
            if ($type->supports($boardingCard)) {
                return new Connection(
                    $type,
                    new SimpleCoordinates($from),
                    new SimpleCoordinates($to),
                    $boardingCard
                );
            }
        }
        
        throw new \RuntimeException(sprintf('Boarding card `%s` is not supported', $boardingCard));
    }
}
