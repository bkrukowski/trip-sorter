<?php

namespace Bartek\TripSorter\Connections;

use Bartek\TripSorter\Coordinates\CoordinatesInterface;
use Bartek\TripSorter\Types\TypeInterface;

class Connection implements ConnectionInterface
{
    private $type;
    
    private $from;
    
    private $to;
    
    private $boardingCard;
    
    public function __construct(TypeInterface $type, CoordinatesInterface $from, CoordinatesInterface $to, string $boardingCard)
    {
        $this->from = $from;
        $this->to = $to;
        $this->type = $type;
        $this->boardingCard = $boardingCard;
    }

    public function getType(): TypeInterface
    {
        return $this->type;
    }

    public function getFrom(): CoordinatesInterface
    {
        return $this->from;
    }

    public function getTo(): CoordinatesInterface
    {
        return $this->to;
    }

    public function describe(): string
    {
        return $this->type->describe($this->from, $this->to, $this->boardingCard);
    }
}
