<?php

declare(strict_types=1);

namespace Bartek\TripSorter\Connections;

use Bartek\TripSorter\Coordinates\CoordinatesInterface;
use Bartek\TripSorter\Types\TypeInterface;

interface ConnectionInterface
{
    /**
     * @return TypeInterface Object represents type of connection, e.g. train, plain, etc.
     */
    public function getType(): TypeInterface;
    
    public function getFrom(): CoordinatesInterface;

    public function getTo(): CoordinatesInterface;
    
    public function describe(): string;
}
