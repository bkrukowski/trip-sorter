<?php

declare(strict_types=1);

namespace Bartek\TripSorter\Connections;

class ConnectionChain implements \IteratorAggregate
{
    private $sorted = false;

    /**
     * @var ConnectionInterface[]
     */
    private $connections = [];

    /**
     * @var ConnectionInterface[]
     */
    private $connectionsByFrom = [];

    /**
     * @var ConnectionInterface[]
     */
    private $connectionsByTo = [];

    /**
     * Returns iterator with sorted connections
     *
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        $this->sort();

        return new \ArrayIterator($this->connections);
    }

    public function addConnection(ConnectionInterface $connection)
    {
        if ($this->connectionsByFrom[$connection->getFrom()->getId()] ?? null) {
            throw new \InvalidArgumentException(
                sprintf('Connection which begins in %s is already added.', $connection->getFrom()->getLabel())
            );
        }

        if ($this->connectionsByTo[$connection->getTo()->getId()] ?? null) {
            throw new \InvalidArgumentException(
                sprintf('Connection which ends in %s is already added.', $connection->getTo()->getLabel())
            );
        }

        $this->connectionsByFrom[$connection->getFrom()->getId()] = $connection;
        $this->connectionsByTo[$connection->getTo()->getId()] = $connection;
        $this->sorted = false;
    }

    private function sort(): void
    {
        if (!$this->connectionsByFrom) {
            $this->sorted = true;
            return;
        }

        /** @var ConnectionInterface $first */
        /** @var ConnectionInterface $last */
        $first = $last = reset($this->connectionsByFrom);
        $resultChain = [$first];

        while (count($resultChain) !== count($this->connectionsByFrom)) {
            $prev = $this->connectionsByTo[$first->getFrom()->getId()] ?? null;
            $next = $this->connectionsByFrom[$last->getTo()->getId()] ?? null;

            if (null !== $prev) {
                $first = $prev;
                array_unshift($resultChain, $prev);
            }

            if (null !== $next) {
                $last = $next;
                array_push($resultChain, $next);
            }

            if (null === $prev && null === $next) {
                throw new \RuntimeException('Broken chain');
            }
        }

        $this->sorted = true;
        $this->connections = $resultChain;
    }
}
