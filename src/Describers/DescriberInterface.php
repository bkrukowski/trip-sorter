<?php

declare(strict_types=1);

namespace Bartek\TripSorter\Describers;

use Bartek\TripSorter\Connections\ConnectionChain;

interface DescriberInterface
{
    public function describeTrip(ConnectionChain $connections): string;
}
