<?php

declare(strict_types=1);

namespace Bartek\TripSorter\Describers;

use Bartek\TripSorter\Connections\ConnectionChain;
use Bartek\TripSorter\Connections\ConnectionInterface;

class Describer implements DescriberInterface
{
    public function describeTrip(ConnectionChain $connections): string
    {
        $result = [];
        $i = 1;
        foreach ($connections as $connection) {
            /** @var ConnectionInterface $connection */
            $result[] = sprintf('% 3d. ', $i++) . str_replace("\n", "\n     ", $connection->describe());
        }
        $result[] = sprintf('% 3d. ', $i). 'You have arrived at your final destination.';
        
        return implode("\n", $result);
    }
}
