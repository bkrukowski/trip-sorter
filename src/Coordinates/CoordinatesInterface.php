<?php

declare(strict_types=1);

namespace Bartek\TripSorter\Coordinates;

interface CoordinatesInterface
{
    /**
     * Human friendly id of place
     * 
     * @return string
     */
    public function getLabel(): string;

    /**
     * Unique id of place
     * 
     * @return int|string
     */
    public function getId();
}
