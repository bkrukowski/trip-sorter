<?php

declare(strict_types=1);

namespace Bartek\TripSorter\Coordinates;

class SimpleCoordinates implements CoordinatesInterface
{
    private $name;
    
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getLabel(): string
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->name;
    }
}
