<?php

namespace Bartek\TripSorter\Connections;

use Bartek\TripSorter\AbstractTest;
use Bartek\TripSorter\Coordinates\CoordinatesInterface;
use Bartek\TripSorter\Coordinates\SimpleCoordinates;
use Bartek\TripSorter\Types\TypeInterface;

/**
 * @internal
 */
class ConnectionTest extends AbstractTest
{
    /**
     * @dataProvider providerAll
     * 
     * @param string $from
     * @param string $to
     * @param string $boardingCard
     */
    public function testAll(string $from, string $to, string $boardingCard)
    {
        $type = $this->createTestType();
        $fromCoord = new SimpleCoordinates($from);
        $toCoord = new SimpleCoordinates($to);
        $connection = new Connection(
            $type,
            $fromCoord,
            $toCoord,
            $boardingCard
        );
        
        $this->assertSame($type, $connection->getType());
        $this->assertSame($fromCoord, $connection->getFrom());
        $this->assertSame($toCoord, $connection->getTo());
        $this->assertSame(
            sprintf('Simple connection from %s to %s [%s]', $from, $to, $boardingCard),
            $connection->describe()
        );
    }
    
    public function providerAll()
    {
        return [
            ['Poznań', 'Buenos Aires', 'my_sample_boarding_card'],
            ['Berlin', 'Dubai', 'my_sample_boarding_card'],
        ];
    }
    
    private function createTestType(): TypeInterface
    {
        return new class () implements TypeInterface
        {
            public function supports(string $boardingCardData): bool
            {
                return true;
            }

            public function describe(CoordinatesInterface $from, CoordinatesInterface $to, string $boardingCard): string
            {
                return sprintf(
                    'Simple connection from %s to %s [%s]',
                    $from->getLabel(),
                    $to->getLabel(),
                    $boardingCard
                );
            }
        };
    }
}
