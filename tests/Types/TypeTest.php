<?php

namespace Bartek\TripSorter\Types;

use Bartek\TripSorter\AbstractTest;
use Bartek\TripSorter\Coordinates\SimpleCoordinates;
use PHPUnit\Util\Type;

/**
 * @internal
 */
class TypeTest extends AbstractTest
{
    /**
     * @dataProvider providerSupports
     * 
     * @param TypeInterface $type
     * @param string        $boardingCard
     * @param bool          $expected
     */
    public function testSupports(TypeInterface $type, string $boardingCard, bool $expected)
    {
        $this->assertSame($expected, $type->supports($boardingCard));
    }
    
    public function providerSupports()
    {
        return [
            [new TrainType(), 'train_432_seat_no', true],
            [new TrainType(), 'tRaIN_432_SEat_no', true],
            [new TrainType(), 'train_432_seat_no_seat', false],
            
            [new FlightType(), 'Flight_312_gate_12_seat_123c_bag_drop_111', true],
            [new FlightType(), 'Flight_312_gate_12_seat_123c_bag_drop_abc', false],
            [new FlightType(), 'Flight_312_gate_12_seat_123c', true],
            
            [new FlightType(), 'train_432_seat_no_seat', false],
        ];
    }

    /**
     * @dataProvider providerDescribe
     * 
     * @param TypeInterface $type
     * @param string        $from
     * @param string        $to
     * @param string        $boardingCard
     * @param string        $expected
     */
    public function testDescribe(TypeInterface $type, string $from, string $to, string $boardingCard, string $expected)
    {
        $description = $type->describe(
            new SimpleCoordinates($from),
            new SimpleCoordinates($to),
            $boardingCard
        );
        
        $this->assertSame($expected, $description);
    }
    
    public function providerDescribe()
    {
        $firstDes = "Take flight [312] from Poznań to Dubai. Gate 12, seat 123c.\nBaggage drop at ticket counter 111.";
        $secondDes = "Take flight [312] from Berlin to Poznań. Gate 12, seat 123c.\nBaggage will we automatically transferred from your last leg.";
        $thirdDes = 'Take train [333] from Porto to Madrid. Seat 421a.';
        
        return [
            [new FlightType(), 'Poznań', 'Dubai', 'Flight_312_gate_12_seat_123c_bag_drop_111', $firstDes],
            [new FlightType(), 'Berlin', 'Poznań', 'Flight_312_gate_12_seat_123c', $secondDes],
            [new TrainType(), 'Porto', 'Madrid', 'train_333_seat_421a', $thirdDes],
        ];
    }
}
