<?php

declare(strict_types=1);

namespace Bartek\TripSorter;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 */
abstract class AbstractTest extends TestCase
{
    protected function setUp()
    {
        $this->expectOutputString('');
    }
}
