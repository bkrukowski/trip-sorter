# TripSorter

## Tests

```bash
composer update
vendor/bin/phpunit
```

## Example

### Code

```php
<?php

use Bartek\TripSorter\Connections\SimpleConnectionFactory;
use Bartek\TripSorter\Connections\ConnectionChain;
use Bartek\TripSorter\Describers\Describer;

$connectionFactory = new SimpleConnectionFactory();
$chain = new ConnectionChain();
$describer = new Describer();

/**
 * Clarification
 * ['Madrid', 'Barcelona', 'train_432_seat_no']
 * 'Madrid' - place of departure
 * 'Barcelona' - place of arrival
 * 'train_432_seat_no' - boarding card ID
 * Factory automatically converts locations passed as string to objects.
 */

$ticketsData = [
    ['Madrid', 'Barcelona', 'train_432_seat_no'],
    ['Berlin', 'Poznań', 'Flight_312_gate_12_seat_123c'],
    ['Barcelona', 'Berlin', 'Flight_312_gate_12_seat_123c_bag_drop_111'],
    ['Porto', 'Madrid', 'train_333_seat_421a'],
];

foreach ($ticketsData as $row) {
    $chain->addConnection($connectionFactory->createConnection(...$row));
}

echo $describer->describeTrip($chain), "\n";
```

### Output

```
  1. Take train [333] from Porto to Madryt. Seat 421a.
  2. Take train [432] from Madryt to Barcelona. No seat assignment.
  3. Take flight [312] from Barcelona to Berlin. Gate 12, seat 123c.
     Baggage drop at ticket counter 111.
  4. Take flight [312] from Berlin to Poznań. Gate 12, seat 123c.
     Baggage will we automatically transferred from your last leg.
  5. You have arrived at your final destination.
```
